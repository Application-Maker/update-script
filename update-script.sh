#!/bin/bash

# Disable certain shellcheck checks for this script
# SC2059: Disable warnings about printf format string
# shellcheck disable=SC2059

# Colors
LRED='\e[1;31m'
LGREEN='\e[1;32m'
NONE='\e[0m'

SCRIPT_PATH="$1"
LINK="$2"

printf "\n${LBLUE}Updating script...$NONE\n"
if curl "$LINK" -o "$SCRIPT_PATH"; then
    printf "\n${LGREEN}Script updated successfully$NONE\n"
else
    printf "\n${LRED}Script update failed, Please check your internet connection and permissions$NONE\n"
    exit 1
fi

shift 2

chmod +x "$SCRIPT_PATH"
# Run the updated script with the remaining arguments
$SCRIPT_PATH "$@"

exit 0